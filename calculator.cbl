      ******************************************************************
      * Author:BRILLANDE
      * Date:26/07/2022
      * Purpose:
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. CALCULATOR.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
       01 NUMBER-INPUT.
           05 WSV-NUMBER1 PIC 9(10).
           05 WSV-NUMBER2 PIC 9(10).

       01 MASC.
           05 WSV-RESULT-EDIT PIC Z(9)9.

       01 WSV-OPERATIONS PIC X.
           88 WSS-ADD VALUE "+".
           88 WSS-SUBTRACT VALUE "-".
           88 WSS-MULTIPLY VALUE "*".
           88 WSS-DIVIDE VALUE "/".

       01 WSV-RESULT PIC 9(11).

       PROCEDURE DIVISION.
       INIT.
           DISPLAY "INPUT A NUMBER".
           ACCEPT WSV-NUMBER1.
           DISPLAY "INPUT + - * OR /".
           ACCEPT WSV-OPERATIONS.
           DISPLAY "INPUT A SECOND NUMBER".
           ACCEPT WSV-NUMBER2.
           PERFORM OPERATIONS.

       OPERATIONS.
           IF WSV-OPERATIONS = "+"
               ADD WSV-NUMBER1 TO WSV-NUMBER2 GIVING WSV-RESULT
               MOVE WSV-RESULT TO WSV-RESULT-EDIT.
           IF WSV-OPERATIONS = "-"
               SUBTRACT WSV-NUMBER1 FROM WSV-NUMBER2 GIVING WSV-RESULT
               MOVE WSV-RESULT TO WSV-RESULT-EDIT.
           IF WSV-OPERATIONS = "*"
               MULTIPLY WSV-NUMBER1 BY WSV-NUMBER2 GIVING WSV-RESULT
               MOVE WSV-RESULT TO WSV-RESULT-EDIT.
           IF WSV-OPERATIONS = "/"
               DIVIDE WSV-NUMBER1 BY WSV-NUMBER2 GIVING WSV-RESULT
               MOVE WSV-RESULT TO WSV-RESULT-EDIT
               ELSE

           DISPLAY WSV-RESULT-EDIT.
           STOP RUN.
       END PROGRAM CALCULATOR.
